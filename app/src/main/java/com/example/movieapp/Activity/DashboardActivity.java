package com.example.movieapp.Activity;

import android.os.Bundle;
import android.view.MenuItem;

import com.example.movieapp.Adapters.ViewPagerAdapter;
import com.example.movieapp.Fragments.WatchLaterFragment;
import com.example.movieapp.Fragments.ProfileFragment;
import com.example.movieapp.Fragments.SearchFragment;
import com.example.movieapp.R;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

//Main page that contains all the principal objects, like BottomNavigationView, and manage the
//fragments
public class DashboardActivity extends AppCompatActivity {

    private ViewPager viewPager;

    //Fragments
    WatchLaterFragment watchLaterFragment;
    SearchFragment searchFragment;
    ProfileFragment profileFragment;
    MenuItem prevMenuItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        //Initializing viewPager
        viewPager = findViewById(R.id.view_pager);

        final BottomNavigationView navigation = findViewById(R.id.bottom_navigation_view);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels)
            { }

            @Override
            public void onPageSelected(int position) {
                if (prevMenuItem != null) {
                    prevMenuItem.setChecked(false);
                } else {
                    navigation.getMenu().getItem(0).setChecked(false);
                }
                navigation.getMenu().getItem(position).setChecked(true);
                prevMenuItem = navigation.getMenu().getItem(position);

                if (position == 0){
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

       /*  //Disable ViewPager Swipe
       viewPager.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View v, MotionEvent event)
            {
                return true;
            }
        });
        */

        setupViewPager(viewPager);
    }


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_favourites:
                    viewPager.setCurrentItem(0);
                    return true;
                case R.id.navigation_search:
                    viewPager.setCurrentItem(1);
                    return true;
                case R.id.navigation_profile:
                    viewPager.setCurrentItem(2);
                    return true;
            }
            return false;
        }
    };

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        watchLaterFragment = new WatchLaterFragment();
        searchFragment = new SearchFragment();
        profileFragment = new ProfileFragment();
        adapter.addFragment(watchLaterFragment);
        adapter.addFragment(searchFragment);
        adapter.addFragment(profileFragment);
        viewPager.setAdapter(adapter);
    }
}
