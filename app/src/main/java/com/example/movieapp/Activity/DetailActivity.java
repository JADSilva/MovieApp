package com.example.movieapp.Activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.movieapp.Classes.APIResponse;
import com.example.movieapp.Controllers.DBController;
import com.example.movieapp.R;

import java.util.ArrayList;

import androidx.appcompat.app.AppCompatActivity;

public class DetailActivity extends AppCompatActivity {


    ImageView posterImage;
    TextView title, year, released, runtime, genre, director, writer, actors, plot, language,
            country, awards, imdbRating, totalSeasons;
    LinearLayout seasonsLayout;
    CheckBox watchLater;
    APIResponse apiResponse;
    SharedPreferences pref;

    DBController mybd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        watchLater = findViewById(R.id.cb_watchLater);
        pref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        mybd = new DBController(this);

        posterImage = findViewById(R.id.iv_poster);
        title = findViewById(R.id.tv_title);
        year = findViewById(R.id.tv_year);
        released = findViewById(R.id.tv_released);
        runtime = findViewById(R.id.tv_runtime);
        genre = findViewById(R.id.tv_genre);
        director = findViewById(R.id.tv_director);
        writer = findViewById(R.id.tv_writer);
        actors = findViewById(R.id.tv_actors);
        plot = findViewById(R.id.tv_plot);
        language = findViewById(R.id.tv_language);
        country = findViewById(R.id.tv_country);
        awards = findViewById(R.id.tv_awards);
        totalSeasons = findViewById(R.id.tv_totalSeasons);
        imdbRating = findViewById(R.id.tv_imdbRating);

        seasonsLayout = findViewById(R.id.l_seasons);

        apiResponse = (APIResponse) getIntent().getSerializableExtra("ItemClicked");

        if (mybd.checkIfRecordExists(apiResponse.getImdbID(), pref.getString(this.getString(R.string.email_sp), "null"))) {
            watchLater.setChecked(true);
        }else{
            watchLater.setChecked(false);
        }

        getSupportActionBar().setTitle("" + apiResponse.getTitle());
        title.setText(apiResponse.getTitle());
        year.setText(apiResponse.getYear());
        released.setText(apiResponse.getReleased());
        runtime.setText(apiResponse.getRuntime());
        genre.setText(apiResponse.getGenre());
        director.setText(apiResponse.getDirector());
        writer.setText(apiResponse.getWriter());
        actors.setText(apiResponse.getActors());
        plot.setText(apiResponse.getPlot());
        language.setText(apiResponse.getLanguage());
        country.setText(apiResponse.getCountry());
        awards.setText(apiResponse.getAwards());
        if (apiResponse.getTotalSeasons() != null && apiResponse.getTotalSeasons() != ""){
            totalSeasons.setText(apiResponse.getTotalSeasons());
        }else{
            seasonsLayout.setVisibility(View.GONE);
        }
        imdbRating.setText(apiResponse.getImdbRating());

        try {
            if (apiResponse.getPoster() == null || apiResponse.getPoster().isEmpty() || apiResponse.getPoster().equals("N/A")){
                posterImage.setImageResource(R.drawable.no_poster);
            }else{
                Glide.with(this).load(apiResponse.getPoster()).into(posterImage);
            }
        } catch (NullPointerException e) {
            Toast.makeText(getApplicationContext(), this.getString(R.string.no_image), Toast.LENGTH_LONG).show();
        }
    }

    public void addFavourite(View view) {
        if (mybd.checkIfRecordExists(apiResponse.getImdbID(), pref.getString(this.getString(R.string.email_sp), null))) {
            mybd.deleteItem(apiResponse.getImdbID());
        }else {
            SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(DetailActivity.this);
            mybd.insertItem(apiResponse, pref.getString(this.getString(R.string.email_sp), "null"));
        }
        this.finish();
    }
}
