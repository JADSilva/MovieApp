package com.example.movieapp.Interfaces;

import org.json.JSONObject;

public interface ServerCallback{
    void onSuccess(JSONObject result);
}
