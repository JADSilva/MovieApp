package com.example.movieapp.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.movieapp.Classes.APIResponse;
import com.example.movieapp.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

//Adapter for the RecyclerViews of the App
public class ItemsAdapter extends
        RecyclerView.Adapter<ItemsAdapter.ViewHolder>  {

    private Context context;
    private OnItemClickListener onItemClickListener;


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View itemView = inflater.inflate(R.layout.item_row, parent, false);

        // Return a new holder instance
        ViewHolder viewHolder = new ViewHolder(itemView, onItemClickListener);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ItemsAdapter.ViewHolder viewHolder, int position) {
        // Get the data model based on position
        APIResponse item = mItems.get(position);

        // Set item views based on views and data model
        TextView name_tv = viewHolder.nameTextView;
        name_tv.setText(item.getTitle());
        TextView year_tv = viewHolder.yearTextView;
        year_tv.setText(item.getYear());

        //Insert image url
        ImageView ivPoster = viewHolder.iv_poster;
        if (item.getPoster() == null || item.getPoster().isEmpty() || item.getPoster().equals("N/A")){
            ivPoster.setImageResource(R.drawable.no_poster);
        }else{
        Glide.with(context).load(item.getPoster()).into(ivPoster);
        }
    }

    @Override
    public int getItemCount() {
        if (mItems != null)
            return mItems.size();
        else
            return 0;
    }

    // Reference to each of the views within a data item
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView nameTextView;
        public TextView yearTextView;
        public ImageView iv_poster;

        OnItemClickListener onItemClickListener;
        // Constructor that accepts the entire item row
        public ViewHolder(View itemView, OnItemClickListener onItemClickListener) {
            super(itemView);

            this.onItemClickListener = onItemClickListener;

            nameTextView = itemView.findViewById(R.id.tv_film_name);
            yearTextView = itemView.findViewById(R.id.tv_film_year);
            iv_poster = itemView.findViewById(R.id.iv_poster);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onItemClickListener.onItemClick(getAdapterPosition());
        }
    }

    private List<APIResponse> mItems;

    // Pass in the contact array into the constructor
    public ItemsAdapter(List<APIResponse> items, OnItemClickListener onItemClickListener) {
        mItems = items;
        this.onItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListener{
        void onItemClick(int position);
    }

}