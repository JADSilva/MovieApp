package com.example.movieapp.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.movieapp.Classes.APIResponse;
import com.example.movieapp.Adapters.ItemsAdapter;
import com.example.movieapp.Controllers.APIController;
import com.example.movieapp.Activity.DetailActivity;
import com.example.movieapp.Interfaces.ServerCallback;
import com.example.movieapp.R;
import com.google.gson.Gson;

import org.json.JSONObject;
import java.util.ArrayList;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


//Fragment that contains the items search functions
public class SearchFragment extends Fragment implements View.OnClickListener, ItemsAdapter.OnItemClickListener {

    ArrayList<APIResponse> items;
    ItemsAdapter adapter;
    private APIController apiController;
    private TextView tvNone;
    private RecyclerView rvItems;
    private Spinner spinner;
    private ProgressDialog dialog;

    public SearchFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.search_fragment, container, false);
        ImageView b = v.findViewById(R.id.bt_search);
        tvNone = v.findViewById(R.id.tv_none);
        b.setOnClickListener(this);
        String [] values =
                {"Movie","Series","Episode"};
        spinner = v.findViewById(R.id.sp_type);

        items = new ArrayList<>();

        dialog = new ProgressDialog(this.getActivity());

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this.getActivity(),
                android.R.layout.simple_spinner_item, values);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        spinner.setAdapter(arrayAdapter);

        // Lookup the recyclerview in activity layout
        rvItems = v.findViewById(R.id.rv_Items);
        // Create adapter passing in the sample user data
        adapter = new ItemsAdapter(items, this);
        // Attach the adapter to the recyclerview to populate items
        rvItems.setAdapter(adapter);
        // Set layout manager to position the items
        rvItems.setLayoutManager(new LinearLayoutManager(getContext()));



        return v;
    }

    @Override
    public void onClick(final View v) {
        //initializing the api controller
        this.apiController = new APIController(getContext());

        InputMethodManager imm =(InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

        dialog.setMessage(getString(R.string.searching));
        dialog.show();
        this.apiController.requestItems(((EditText) getView().findViewById(R.id.et_search)).getText().toString(),
                ((EditText) getView().findViewById(R.id.et_year)).getText().toString(),
                spinner,new ServerCallback() {
                    @Override
                    public void onSuccess(JSONObject response) {

                        if(items != null){
                            items.clear();
                        }

                        Gson g = new Gson();
                        APIResponse apiResp = g.fromJson(response.toString(), APIResponse.class);
                        if(apiResp.getTitle() != "" && apiResp.getTitle() != null){
                            items.add(apiResp);
                            adapter.notifyDataSetChanged();
                            rvItems.setVisibility(View.VISIBLE);
                            tvNone.setVisibility(View.GONE);
                        } else {
                            tvNone.setVisibility(View.VISIBLE);
                            rvItems.setVisibility(View.GONE);
                        }
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }

                    }
                }
        );
    }

    @Override
    public void onItemClick(int position) {
        items.get(position);
        Intent i = new Intent(getContext(), DetailActivity.class);
        i.putExtra("ItemClicked", items.get(position));
        startActivity(i);
    }
}
