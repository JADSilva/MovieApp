package com.example.movieapp.Fragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.movieapp.Classes.APIResponse;
import com.example.movieapp.Adapters.ItemsAdapter;
import com.example.movieapp.Controllers.DBController;
import com.example.movieapp.Activity.DetailActivity;
import com.example.movieapp.R;

import java.util.ArrayList;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

//Fragment that contains the watch later list, with all the items previous selected by a certain
// authenticated user
public class WatchLaterFragment extends Fragment implements View.OnClickListener, ItemsAdapter.OnItemClickListener {

    ArrayList<APIResponse> items;
    ItemsAdapter adapter;
    private RecyclerView rvItems;
    private TextView tvNone;
    private Spinner sp_later;
    private ConstraintLayout cl_filter;
    private final String [] values =  {"All","Movie","Series","Episode"};

    SharedPreferences pref;

    DBController mybd;

    public WatchLaterFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.watch_later_fragment, container, false);

        rvItems = v.findViewById(R.id.rv_WatchLaterItems);
        tvNone = v.findViewById(R.id.tv_none);
        cl_filter = v.findViewById(R.id.cl_filter);

        pref = PreferenceManager.getDefaultSharedPreferences(getContext());
        mybd = new DBController(getContext());
        items = mybd.getAllItems(pref.getString(this.getString(R.string.email_sp), "null"));

        refreshView();

        sp_later = v.findViewById(R.id.sp_later);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this.getActivity(), android.R.layout.simple_spinner_item, values);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        sp_later.setAdapter(arrayAdapter);

        sp_later.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {

                if(position == 0){
                    items = mybd.getAllItems(pref.getString(getContext().getString(R.string.email_sp), "null"));
                }else {
                    items = mybd.getFilteredItems(values[position],pref.getString(getContext().getString(R.string.email_sp), "null"));
                }
                iniRecyclerView();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        return v;
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        mybd = new DBController(getContext());


        items = mybd.getAllItems(pref.getString(this.getString(R.string.email_sp), "null"));

        refreshView();

        iniRecyclerView();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onClick(View v) {
    }

    @Override
    public void onItemClick(int position) {

        Intent i = new Intent(getContext(), DetailActivity.class);
        i.putExtra("ItemClicked", items.get(position));
        startActivity(i);
    }

    public void refreshView(){
        if (items.size() > 0) {

            cl_filter.setVisibility(View.VISIBLE);
            rvItems.setVisibility(View.VISIBLE);
            tvNone.setVisibility(View.GONE);
            iniRecyclerView();

        } else {
            cl_filter.setVisibility(View.GONE);
            tvNone.setVisibility(View.VISIBLE);
            rvItems.setVisibility(View.GONE);
        }
    }

    public void iniRecyclerView(){
        // Create adapter passing in the data
        adapter = new ItemsAdapter(items, this);
        // Attach the adapter to the recyclerview to populate items
        rvItems.setAdapter(adapter);
        // Set layout manager to position the items
        rvItems.setLayoutManager(new LinearLayoutManager(getContext()));

        if(items.size()==0){
            tvNone.setVisibility(View.VISIBLE);
        }else {
            tvNone.setVisibility(View.GONE);
        }
    }
}
