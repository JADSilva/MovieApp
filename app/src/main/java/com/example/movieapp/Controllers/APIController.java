package com.example.movieapp.Controllers;

import android.content.Context;
import android.widget.Spinner;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.movieapp.Interfaces.ServerCallback;

import org.json.JSONObject;

//API Controller that makes all the requests needed for the view filling
public class APIController {

    private Context appContext;
    private static String URL = "https://www.omdbapi.com/?";
    private static String API_KEY = "&apikey=3cff3f4";
    private static String imdbID = "i=";
    private static String itemTitleSearch = "t=";
    private static String itemType = "type=";
    private static String itemYear = "y=";
    private static String yearOfRelease = "y=";
    private static String[] plot = new String[]{"short","full"};
    private static String[] r = new String[]{"json","xml"};

    public APIController(Context applicationContext) {
        this.appContext = applicationContext;
    }

    public void requestItems(final String filmTitle, final String year, Spinner spinner, final ServerCallback callback)
    {
        //Formating the title to add '+' to the query parameter
        String query = this.formatQuery(filmTitle);
        // Initialize a new RequestQueue instance
        RequestQueue requestQueue = Volley.newRequestQueue(appContext);

        //Formating query with the item type (moview, series, episode)
        if( spinner!= null && spinner.getSelectedItem() !=null ) {
            query += "&" + itemType + spinner.getSelectedItem().toString().toLowerCase();
        }

        //Formating query with the item year
        if( year != null && year.equals("") !=true ) {
            query += "&" + itemYear + year;
        }

        //Request filtered data in JSON
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                URL + itemTitleSearch +  query + API_KEY,
                null, new Response.Listener<JSONObject>()
        {

            @Override
            public void onResponse(JSONObject response) {
                callback.onSuccess(response); // call call back function here

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            public String getBodyContentType()
            {
                return "application/json";
            }
        };

        // Adding request to request queue
        requestQueue.add(jsonObjReq);

    }


    //Formatting the title of items by replacing the blanks with the '+' symbol
    public String formatQuery(String filmTitle){
        String[] filmWords = filmTitle.split(" ");
        String queryFilm = "";
        for (String word : filmWords) {
            if (queryFilm.length() > 0){
                queryFilm += "+" + word;
            }else if (queryFilm.length() == 0){
                queryFilm += word;
            }
        }

        return queryFilm;

    }

}
