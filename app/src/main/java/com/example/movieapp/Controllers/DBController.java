package com.example.movieapp.Controllers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.movieapp.Classes.APIResponse;

import java.util.ArrayList;

//Data Base Controller that controls all the storage management of the data
public class DBController extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "MOVIE_SERIES.db";
    public static final String ITEM_OWNER = "item_owner";
    public static final String ITEM_TITLE = "title";
    public static final String ITEM_YEAR = "year";
    public static final String ITEM_RELEASED = "released";
    public static final String ITEM_RUNTIME = "runtime";
    public static final String ITEM_GENRE = "genre";
    public static final String ITEM_DIRECTOR = "director";
    public static final String ITEM_WRITER = "writer";
    public static final String ITEM_ACTORS = "actors";
    public static final String ITEM_PLOT = "plot";
    public static final String ITEM_LANGUAGE = "language";
    public static final String ITEM_COUNTRY = "country";
    public static final String ITEM_AWARDS = "awards";
    public static final String ITEM_POSTER = "poster";
    public static final String ITEM_TOTAL_SEASONS = "totalSeasons";
    public static final String ITEM_IMDB_RATING = "imdbRating";
    public static final String ITEM_IMDBID = "imdbID";
    public static final String ITEM_TYPE = "type";


    public DBController(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    //DB Creation
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                "create table items " +
                        "(rowid integer PRIMARY KEY AUTOINCREMENT, imdbID text, item_owner text, title text,year text,released text, runtime text" +
                        ",genre text,director text,writer text,actors text,plot text,language text," +
                        "country text,awards text,poster text,totalSeasons text,imdbRating text, type text)"
        );
    }

    //DB Upgrade
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        db.execSQL("DROP TABLE IF EXISTS items");
        onCreate(db);
    }

    //DB Insertion of an Item
    public boolean insertItem(APIResponse apiResponse, String itemOwner) {
        if (!checkIfRecordExists(apiResponse.getImdbID(), itemOwner)) {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put(ITEM_IMDBID, apiResponse.getImdbID());
            contentValues.put(ITEM_OWNER, itemOwner);
            contentValues.put(ITEM_TITLE, apiResponse.getTitle());
            contentValues.put(ITEM_YEAR, apiResponse.getYear());
            contentValues.put(ITEM_RELEASED, apiResponse.getReleased());
            contentValues.put(ITEM_RUNTIME, apiResponse.getRuntime());
            contentValues.put(ITEM_GENRE, apiResponse.getGenre());
            contentValues.put(ITEM_DIRECTOR, apiResponse.getDirector());
            contentValues.put(ITEM_WRITER, apiResponse.getWriter());
            contentValues.put(ITEM_ACTORS, apiResponse.getActors());
            contentValues.put(ITEM_PLOT, apiResponse.getPlot());
            contentValues.put(ITEM_LANGUAGE, apiResponse.getLanguage());
            contentValues.put(ITEM_COUNTRY, apiResponse.getCountry());
            contentValues.put(ITEM_AWARDS, apiResponse.getAwards());
            contentValues.put(ITEM_POSTER, apiResponse.getPoster());
            contentValues.put(ITEM_TOTAL_SEASONS, apiResponse.getTotalSeasons());
            contentValues.put(ITEM_IMDB_RATING, apiResponse.getImdbRating());
            contentValues.put(ITEM_TYPE, apiResponse.getType());
            db.insert("items", null, contentValues);
        }
        return true;
    }

    //Removal of one item
    public Integer deleteItem(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete("items",
                "imdbID = ? ",
                new String[]{id});
    }

    //Retrieving all items of a certain authenticated user
    public ArrayList<APIResponse> getAllItems(String user) {
        ArrayList<APIResponse> array_list = new ArrayList<APIResponse>();
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "select * from items" + " where " + ITEM_OWNER + " = '" + user + "'";
        Cursor res = db.rawQuery(query, null);

        if (res.moveToFirst()) {
            while (res.isAfterLast() == false) {
                APIResponse apiResponse = new APIResponse(res.getString(res.getColumnIndex(ITEM_TITLE)),
                        res.getString(res.getColumnIndex(ITEM_YEAR)),
                        res.getString(res.getColumnIndex(ITEM_RELEASED)),
                        res.getString(res.getColumnIndex(ITEM_RUNTIME)),
                        res.getString(res.getColumnIndex(ITEM_GENRE)),
                        res.getString(res.getColumnIndex(ITEM_DIRECTOR)),
                        res.getString(res.getColumnIndex(ITEM_WRITER)),
                        res.getString(res.getColumnIndex(ITEM_ACTORS)),
                        res.getString(res.getColumnIndex(ITEM_PLOT)),
                        res.getString(res.getColumnIndex(ITEM_LANGUAGE)),
                        res.getString(res.getColumnIndex(ITEM_COUNTRY)),
                        res.getString(res.getColumnIndex(ITEM_AWARDS)),
                        res.getString(res.getColumnIndex(ITEM_POSTER)),
                        res.getString(res.getColumnIndex(ITEM_TOTAL_SEASONS)),
                        res.getString(res.getColumnIndex(ITEM_IMDB_RATING)),
                        res.getString(res.getColumnIndex(ITEM_IMDBID)),
                        res.getString(res.getColumnIndex(ITEM_TYPE)));
                array_list.add(apiResponse);
                res.moveToNext();
            }
        }
        return array_list;
    }

    //Check if a certain item is already in the watch later list of a certain autheticated user
    public boolean checkIfRecordExists(String imdbID, String user) {
        SQLiteDatabase sqldb = this.getReadableDatabase();
        String query = "Select * from items" + " where " + ITEM_IMDBID + " = '" + imdbID + "'" + " and " + ITEM_OWNER + " = '" + user + "'";
        Cursor cursor = sqldb.rawQuery(query, null);
        if (cursor.getCount() <= 0) {
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }

    //Retrieving a filtered list of items
    public ArrayList<APIResponse> getFilteredItems(String filter, String user) {
        ArrayList<APIResponse> array_list = new ArrayList<APIResponse>();

        SQLiteDatabase db = this.getReadableDatabase();
        String query = "Select * from items" + " where " + ITEM_TYPE + " = '" + filter.toLowerCase() + "'" + " and " + ITEM_OWNER + " = '" + user + "'";
        Cursor res = db.rawQuery(query, null);
        if (res.moveToFirst()) {
            while (res.isAfterLast() == false) {
                APIResponse apiResponse = new APIResponse(res.getString(res.getColumnIndex(ITEM_TITLE)),
                        res.getString(res.getColumnIndex(ITEM_YEAR)),
                        res.getString(res.getColumnIndex(ITEM_RELEASED)),
                        res.getString(res.getColumnIndex(ITEM_RUNTIME)),
                        res.getString(res.getColumnIndex(ITEM_GENRE)),
                        res.getString(res.getColumnIndex(ITEM_DIRECTOR)),
                        res.getString(res.getColumnIndex(ITEM_WRITER)),
                        res.getString(res.getColumnIndex(ITEM_ACTORS)),
                        res.getString(res.getColumnIndex(ITEM_PLOT)),
                        res.getString(res.getColumnIndex(ITEM_LANGUAGE)),
                        res.getString(res.getColumnIndex(ITEM_COUNTRY)),
                        res.getString(res.getColumnIndex(ITEM_AWARDS)),
                        res.getString(res.getColumnIndex(ITEM_POSTER)),
                        res.getString(res.getColumnIndex(ITEM_TOTAL_SEASONS)),
                        res.getString(res.getColumnIndex(ITEM_IMDB_RATING)),
                        res.getString(res.getColumnIndex(ITEM_IMDBID)),
                        res.getString(res.getColumnIndex(ITEM_TYPE)));
                array_list.add(apiResponse);
                res.moveToNext();
            }
        }
        return array_list;
    }
}