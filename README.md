# MovieApp :movie_camera: :clapper: :iphone:

This project was carried out as part of a test to apply for a job at BOSCH.


## Functionalities

The main objective of this project was the development of an application that provided data from an API, intuitively to the users and in the best way that the developer was able to accomplish. The API provided data about movie/series.
Therefore an Android application was developed that allowed users to browse movies/series and add them to a list to watch later.

To acomplish this ideas the app contains the following functionalities:

* Authentication; 
* Watch later list;
* Search Movies/Series/Episodes;
* Filter search Movies/Series/Episodes list;
* Filter watch later list;

## Built With
The list below presents the main technologies applied by the developer in this application.

* **Layouts** 
    * Linear Layout;
    * Frame Layout;
    * Constraint Layout;
    * Recycler View;
    * View Pager;
    * Bottom Navigation View;
    * Constraint Layout;
    * Scroll View;
    * CardView;
    * Fragments;
    * Glide (Image loading and caching library for Android);
* **Login**
    * Google Firebase Authentication;
* **Communication**
    * Volley (HTTP Requests);
    * JSON (Requests);
* **Storage**
    * SQLite (Data Base);
    * Shared Preferences;




## Built With

* [JAVA](https://www.java.com/pt_BR/) - The web from the technology*
* [Android](https://developer.android.com) - Technology Documentation for developers*
* [Android Studio](https://developer.android.com/studio) - Development environment*
* [OMDB API](http://www.omdbapi.com) - API used to supply data*




## Authors

* **Jorge Silva** - *Design and Development work*
